# Development Environments

We want our development team members to work within a consistent development environment so that we can maintain team standards, and make it easier for our team members to work together.

These development environment settings can take place in many places, from the IDE that the team is using, to containerized development environments running on the developers' local computers, to cloud-based environments in which the developers work. We can also provide scripts that make it easy for developers to perform more complex, repeated tasks such as building, linting, and testing code.

In this activity we will start with easy IDE settings and extensions, and move toward containerized development environments run locally and in the cloud, and finally scripting.

The focus of our work in this activity will be on Visual Studio Code, but these types of customizations can be done with most IDEs.

## Content Learning Objectives

By the end of this activity, participants will be able to...

## Process Skill Goals

_During the activity, students should make progress toward:_

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1 - Install Visual Studio Code

### Model 1 - Instructions

Install Visual Studio Code on your local machine.

1. Go to [https://code.visualstudio.com/download](https://code.visualstudio.com/download)
2. Download the appropriate installer for your machine.
3. Install Visual Studio Code.

## Model 2 - Visual Studio Code Settings

Reference: [Visual Studio Code Settings](https://code.visualstudio.com/docs/getstarted/settings)

Each developer may have preferred settings for their IDE, such as themes, fonts, etc. These are called `User Settings` in Visual Studio Code, and apply any time you open VSCode on your computer. You may also have these [settings synced](https://code.visualstudio.com/docs/getstarted/settings#_settings-sync) between multiple computers.

There are also [`Workspace Settings`](https://code.visualstudio.com/docs/getstarted/settings#_workspace-settings) that are saved with a particular VSCode project. These are settings that will be shared by any developer working in this project.

### Model 2 - Instructions and Questions

1. Open your VSCode user settings. You can do this from the gear icon in the lower-left menu, with `Ctrl+,`, or from the command palette (`Ctrl+Shift+P Preferences: Open Settings (UI)`). Make sure you are on the `User` tab.
2. Spend some time looking through the available settings. Make a list of the sections below:

    -

3. Decide on a section for each of you to look at, and make a note of any you might be interested in modifying later.

    -

4. Your user settings are stored in a `settings.json` file. You can open this file from the command palette with (`Ctrl+Shift+P Preferences: Open Settings (JSON)`). Take a quick look at this file. Look at the [JSON Syntax example](https://en.wikipedia.org/wiki/JSON#Syntax) and [Data types](https://en.wikipedia.org/wiki/JSON#Data_types) in the Wikipedia article on JSON. Try to identify one of each of the types of data types in your `settings.json` file and paste an example below:

    - Number
    - String
    - Boolean
    - Array
    - Object
    - null

5. Open the `Workspace Settings` in the UI. This should look pretty much the same as your `User Settings`. This makes it hard to find out which settings are your, and which are the project's. In the `Search Settings` box, enter `@modified` and you will see the project-specific settings. List them below:
6. These settings are stored in their own `settings.json` file that is stored in the project's `.vscode` folder. Open that file and compare it to the list you saw when filtering by `@modified`.
7. Many tools expect there to be a single blank line at the end of a file. [Markdownlint](https://github.com/DavidAnson/markdownlint) is one of them. It will complain if you do not have a blank line at the end of your file, or if you have more than one.
    1. Try deleting the blank line at the end of this file. You should see a red "squiggly" under the last character of that line. If you hover over it you will see the message from Markdownlint.
    2. Try adding two blank lines and check for the message.
    3. Here is where a workspace setting can be useful for your team members. Open the Workspace Settings, and search for "newline". Enable `Files: Insert Final Newline` and `Files: Trim Final Newlines`.
    4. Try removing the final blank line at the end of this file, then save it. What happened?
    5. Try adding multiple blank lines at the end of this file, then save it. What happened?
    6. Open `.vscode/settings.json` again and see chat has changed.

## Model 3 - Visual Studio Code Extensions

Reference: [Visual Studio Code Extensions](https://code.visualstudio.com/docs/editor/extension-marketplace)

Visual Studio Code Extensions add new functionality. These extensions can be written by third party developers, and users can choose which they wish to install.

Unlike with settings, there is no concept of User vs Workspace extensions. All extensions are installed for the user. However, you can disable an extension temporarily for a workspace.

Projects can recommend extensions to be used with a specific workspace. The user will need to agree to install them.

### Model 3 - Instructions and Questions

1. Open the Extensions pane in Visual Studio Code. Use `Ctrl-Shift-X` or click on the four boxes icon in the left menu.
2. Install the 4 recommended extensions.
3. Close VSCode and reopen the activity. You will probably be asked to install the recommended extensions again.
4. Open your VSCode user settings. You can do this from the gear icon in the lower-left menu, with `Ctrl+,`, or from the command palette (`Ctrl+Shift+P Preferences: Open Settings (UI)`). You should notice that there are extension settings in both `User` and `Workspace`.

## Model 4 - Differences in Individuals' Development Environments

Unfortunately, developers on your team may all be running different operating systems and have different versions of dependencies installed. That means that software developed and running fine on one person's computer may not work on someone else's. This is know as the "It works on my computer." problem.

### Model 4 - Instructions and Questions

1. Every member of your team should open a terminal and run the commands below. Record the results.

    ```bash
    java --version
    javac --version
    ```

2. Compare your results.
    - How many members of your team are missing the Java runtime (`java`)?
    - How many members of your team are missing the Java compiler (`javac`)?
    - How many different versions of `java` are installed?
    - How many different versions of `javac` are installed?

## Model 5 - Install Docker Desktop

Containerization has helped to solve this problem by letting us create containers that have installed a particular operating system and dependencies, all at particular version numbers. We do this with tools like Docker.

### Model 5 - Instructions

Install Docker Desktop on your local machine.

1. Go to [https://www.docker.com/products/docker-desktop/](https://www.docker.com/products/docker-desktop/)
2. Download the appropriate installer for your machine.
3. Install Docker Desktop.

## Model 6 - Install git

### Model 6 - Instructions

Install git on your local machine.

1. Go to [https://git-scm.com/](https://git-scm.com/)
2. Download the appropriate installer for your machine.
3. Install git.

## Model 7 - Visual Studio Code Dev Containers

Reference: [Developing inside a Container](https://code.visualstudio.com/docs/devcontainers/containers)

Visual Studio Code has gone a step further by letting you create Dev Containers that include not only the OS and dependencies, but also a VSCode server inside the container that you connect to over ssh from your locally running copy of VSCode.

![](https://code.visualstudio.com/assets/docs/devcontainers/containers/architecture-containers.png) From https://code.visualstudio.com/docs/devcontainers/containers)

### Model 7 - Instructions and Questions

1. Make sure every member of your team has the most recent version of this activity cloned to their computer.
    1. Open a terminal
    2. `git clone https://gitlab.com/worcester/cs/cs-348-01-02-e1-fall-2024/in-class-activities/development-environments.git`
2. Open this activity in VSCode.
3. Click on the `Open a Remote Window` button (`><`) in the lower left of your screen.
4. Choose `Add Dev Container Configuration Files...`
    1. Choose `Add configuration to workspace`.
    2. In the search box type `java`
    3. Choose the `Java devcontainers` option
    4. For `Java version` choose the default
    5. Leave the `Select the options for the 'Jaca' dev container` unchecked and click `OK`
    6. Leave the `Select additional features to install` unchecked and click `OK`
    7. Leave the `Include the following optional files/directories` unchecked and click `OK`
    8. Click `Reopen in container` when prompted.
    9. This will take a little while to build and open the container.
    10. **Do not install the recommended extensions when prompted.**
5. Notice that you now have a `.devcontainer` directory with a `devcontainer.json` file in it.
6. Every member of your team should open a terminal and run the commands below. Record the results.

    ```bash
    java --version
    javac --version
    ```

7. Compare your results.
    - How many members of your team are missing the Java runtime (`java`)?
    - How many members of your team are missing the Java compiler (`javac`)?
    - How many different versions of `java` are installed?
    - How many different versions of `javac` are installed?
8. Open `.devcontainer/devcontainer.json`. Can you find the Java version that was installed somewhere in this file? Where?
9. Open the Extensions pane in Visual Studio Code. Use `Ctrl-Shift-X` or click on the four boxes icon in the left menu.
    1. Notice the extensions listed under `Workspace Recommendations`. Where have you seen these listed before? in the project?
    2. Notice that these are all grayed-out and have a `Install in Dev Container: Jav...` button. Click the button for each one to install it.
10. Click on the `Open a Remote Window` button (`><`) in the lower left of your screen, and choose `Rebuild Container`.
    1. This will take a little while to build and open the container.
    2. Notice that it still asks you to if you want to install the recommended extensions. **Do not install the recommended extensions when prompted.**
11. You need to install the extensions into the `devcontainer.json` file to have them automatically installed when the container starts.
    1. Open the extensions pane again. Find the `Code Spell Checker extension` and, instead of clicking `Install in Dev Container: Jav...`, click on the gear icon and choose `Add to devcontainer.json`.
    2. When you are prompted to rebuild the container, choose `Ignore` (for now...)
    3. Open `.devcontainer.json`. Notice that there is a new section for customizations. What has been added?
    4. You could do the same for `markdownlint`, `AlexJS Linter`, and `PlantUML`. But don't!
    5. You can also copy them from the `.vscode/extensions.json` file and paste them directly into the `.devcontainer/devcontainer.json` file. Do this.
12. Open your VSCode user settings. You can do this from the gear icon in the lower-left menu, with `Ctrl+,`, or from the command palette (`Ctrl+Shift+P Preferences: Open Settings (UI)`). Make sure you are on the `User` tab.
    1. Notice that there is now a third tab named `Remote [Dev Container: Java @ desktop-linux]`. Click on this tab, and check for `@modified` settings.
    2. Click on the `Worskpace` tab. Are your workspace settings from Model 1 shown?
13. You can also add your settings to the `.devcontainer/devcontainer.json` file.
    1. Open the `.devcontainer/devcontainer.json` file.
    2. Add this after the `extensions` list:

    ```json
    "settings": {
        "files.insertFinalNewline": true,
        "files.trimFinalNewlines": true,
        "plantuml.render": "PlantUMLServer",
        "plantuml.server": "https://www.plantuml.com/plantuml"
    }
    ```

    3. You probably got red squigglies under `settings`. Hover over and read the message, then fix the syntax error.
14. Save your files and rebuild the container.
15. Check the extensions pane and settings UI to see if your extensions and settings are there.
16. Test your Java Dev Container:
    1. Compile the sample program `javac CommandLineArguments.java`
    2. Run the sample program `java CommandLineArguments a b c d`

Note that you are no longer asked whether you want in install the recommended extensions. This is how you can guarantee installation of extensions and settings for team members using your development environment.

We can install other tools and dependencies by modifying the `.devcontainer/devcontainer.json` file.

## Model 8 - Gitpod

[Gitpod](https://www.gitpod.io/) is a cloud development environment that lets you develop in pre-built development containers, running on their cloud infrastructure, and use a variety of IDEs in your browser.  This means that you do not need to install Docker or an IDE, and your development does not depend on your local machine's resources.

By default, their container image includes many common dependencies:

> By default, Gitpod uses a standard Docker Image called Workspace-Full as the foundation for workspaces. Workspaces started based on this default image come pre-installed with Docker, Nix, Go, Java, Node.js, C/C++, Python, Ruby, Rust, Clojure as well as tools such as Homebrew, Tailscale, Nginx and several more.
https://www.gitpod.io/docs/configure/workspaces/workspace-image

You can add other dependencies with a Docker file, or you can use a Docker image you have made publicly available.

For the `CommandLineArguments.java` example you do not need anything other than what is in the Gitpod default image.

Currently, Gitpod provides 50 free hours per month per user. An additional 100 hours can be purchased for $9 per month. We will not need to purchase any additional time to test this.

### Model 8 - Instructions and Questions

1. Open this activity in your browser on GitLab: https://gitlab.com/worcester/cs/cs-348-01-02-e1-fall-2024/in-class-activities/development-environments.git
2. Prefix the URL for this activity with `https://gitpod.io/#` and open the link. (The link should look like:
https://gitpod.io/#https://gitlab.com/worcester/cs/cs-348-01-02-e1-fall-2024/in-class-activities/development-environments.git)
3. You will need to authenticate with your GitLab account.
4. When it shows you the `New Workspace` page, click `Continue`.
5. Your browser will open a window running VSCode, connected to the Gitpod cloud container.
6. Notice that there is no `.devcontainer` directory. We are not using the Dev Container we built in Model 3, we are using Gitpod's default workspace.
7. Gitpod will load your `.vscode` directory for extensions and settings.
8. Check the extensions pane, and the settings UI to see that your extensions and settings are there.
9. Run the following commands:

    ```bash
    java --version
    javac --version
    ```

    1. How do the results differ from Model 3?
    2. What can you see as a possible problem with using Gitpod's default workspace image?
10. Compile and run the `CommandLineArguments.java` program.
11. **When finished, click the `Gitpod` button in the lower left of your screen and choose `Gitpod: Stop Workspace`. This is very important because if you do not you will use up your hours as your environment continues to run.**

## Model 9 - Command scripts

Some tools we want to have our team standardize on are not available as extensions for our IDE.

To run these, they either have to be installed (in our development container) or run in a Docker image. Generally, they are run at the command line (in our terminal) and/or in a pipeline when you push your changes to GitLab (or similar services). We will look at setting up pipelines in a later activity.

To run these tools at the command line, it is often easier to build scripts to run them. The script can hide the details of running the tool and make it easier for our team members to run them without having to remember those details. We can also have scripts that run multiple tools with one command.

Since our development container has `bash` installed, and so will our pipeline, it makes sense to build these as `bash` scripts.

### A `build` script

It is convenient to have a script to build our project. This makes more sense for projects that are complicated to build (lots of files to compile, many compiler options, libraries to include, etc.) because then we don't have to remember all the details and the order in which to run multiple commands.

1. Create a new directory in named `bin`.
2. In the `bin` directory create a new file named `build.sh`.
3. In `build.sh` add the following lines:

```bash
#!/usr/bin/env bash

javac CommandLineArguments.java
```

    1. What do each of the lines do?

4. Make your script executable with the command `chmod +x build.sh`
5. From your terminal, in the top-level directory, run the script `bin/build.sh`.
6. Change to the `bin` directory in your terminal. Run the script `./build.sh`.
    1. Why did you have to use `./` in front of `build.sh`?
    2. What happened that you did not expect? Why?
7. If we always want to have the script know where the file to compile is, we should put the path to the java file in the command. Edit your script and put `../` in front of the java file's name.
    1. Why did we have to use `../`?
8. Change back to the top-level directory and run the script again. Why didn't it work this time?
9. Let's make sure the script always executes from the directory where the script lives. Add the following lines between the original two lines in your script:

```bash
SCRIPT_DIR="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
cd "$SCRIPT_DIR/.." || exit
```

10. If we had multiple `.java` files that needed to be compiled, what would we have to change in our script?

### A `lint` script

[Linters](https://en.wikipedia.org/wiki/Lint_(software)) are tools that check formatting and style of code and files in our projects. We already have some extensions that do some of this checking, but there are other tools not available as extensions.

An example of this is [Markdown Link Check](https://github.com/tcort/markdown-link-check) that checks to make sure that all the links in your Markdown files are not broken.


**If you are working in your Dev Container (rather than in Gitpod), you will have to modify your Dev Container to allow you to run Docker commands inside the container. Add the following to your `features` in `.devcontainer/devcontainer.json`:***

`"ghcr.io/devcontainers/features/docker-in-docker:2": {}`

**You will need a comma at the end of the line before this.**

1. From the top-level directory, run:
```bash
docker run --rm -v "${PWD}":/tmp:ro -w /tmp \
     ghcr.io/tcort/markdown-link-check:stable -q \
     $(find . -name '*.md' -printf '%p ')
```

Running this will pull the Docker image for Markdown Link Check and run it on all `.md` files.

2. You wouldn't want to copy and paste this everytime, so let's turn it into a script. Create a `lint.sh` file inside your `bin` directory.
3. Make it executable.
4. Add the first 4 lines (including the blank line) you added to `build.sh`.
5. Paste the three lines from step 1 at the end of the file.
6. Save it and run it.
7. We could add another tool to it with this:

```bash
docker run -v "${PWD}:/workdir" \
    ghcr.io/streetsidesoftware/cspell:latest --no-progress \
    "**"
```

8. Save and run it.

---

Copyright © 2024 Karl R. Wurst. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
